#include "bmp.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    FILE* file;
    enum open_status ofr = open_file_read(&file, argv[1]);
    if (ofr != OPEN_OK) {
        printf("File couldn't be opened for reading\n");
        return -1;
    }
    struct image image;
    enum read_status rs = from_bmp(file, &image);
    if (rs != READ_OK) {
        printf_read(from_bmp(file, &image));
        return -1;
    }

    enum close_status cs = close_file(file);
    if (cs != CLOSE_OK) {
        printf("Source file couldn't be closed\n");
        return -1;
    }

    struct image rotated = rotate(image);
    delete_img(&image);

    FILE* out = {0};
    enum open_status ofw = open_file_write(&out, argv[2]);

    if (ofw != OPEN_OK) {
        printf("File couldn't be opened for writing\n");
        return -1;
    }

    enum write_status ws = to_bmp(out, &rotated);
    if (ws != WRITE_OK) {
        printf_write(to_bmp(out, &rotated));
        return -1;
    }

    enum close_status cfo = close_file(out);
    if (cfo != CLOSE_OK) {
        printf("Output file couldn't be closed\n");
        return -1;
    }

    delete_img(&rotated);
    
    return 0;
}
