#include "image.h"
#include "rotate.h"
#include <stdlib.h>


struct image rotate( struct image const source ) {
    struct image rotated; //= create_img(source.height, source.width);
    struct pixel* data = malloc(sizeof (struct pixel) * source.width * source.height);

    rotated.width = source.height;
    rotated.height = source.width;

    uint64_t height = source.height;
    uint64_t width = source.width;
    for (uint64_t i = 0; i < width; i++) {
        for (uint64_t j = 0; j < height; j++) {
            data[i * rotated.width + j] = source.data[(height - 1 - j) * (width) + i];
        }
    }
    rotated.data = data;
    return rotated;
}
