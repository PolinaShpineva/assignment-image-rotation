#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#define BTYPE 0x4d42
#define BISIZE 40
#define BPLANES 1
#define BITCOUNT 24


static struct bmp_header create_header( const struct image* img )
{
    struct bmp_header header = {
        .bfType = BTYPE,
        .bfileSize = (img->width) * (img->height) * sizeof(struct pixel) + (padding(img) * (img->height)) + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BISIZE,
        .biWidth = (uint32_t)(img->width),
        .biHeight = (uint32_t)(img->height),
        .biPlanes = BPLANES,
        .biBitCount = BITCOUNT,
        .biCompression = 0,
        .biSizeImage = (img->width) * (img->height) * sizeof(struct pixel) + padding(img) * (img->height),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    return header;
}


static enum read_status check_header( struct bmp_header* header ) {
    if (header->bfType != BTYPE)
        return READ_INVALID_SIGNATURE;
    if (header->biBitCount != BITCOUNT)
        return READ_INVALID_BITS;
    if (header->biClrUsed != 0)
        return READ_INVALID_COLOR;
    if (header->biSize != BISIZE)
        return READ_INVALID_HEADER_SIZE;
    return READ_OK;

}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (check_header(&header)) {
        return check_header(&header);
    }

    //*img = create_img(header.biWidth, header.biHeight);
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    uint64_t img_padding = padding(img);
    uint64_t c = 0;
    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            fread(img->data + c, sizeof(struct pixel), 1, in);
            c++;
        }
        fseek(in, img_padding, SEEK_CUR);
    }
    
    return READ_OK;
}

enum write_status to_bmp( FILE* out, const struct image* img ) {
    struct bmp_header header = create_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    uint64_t img_padding = padding(img);
    uint8_t bt_padding = 0;
    uint64_t c = 0;
    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            if (fwrite(img->data + c, sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
            c++;
        }
        for (uint64_t k = 0; k < img_padding; k++) {
            if (fwrite(&bt_padding, sizeof(uint8_t), 1, out) != 1)
                return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}







