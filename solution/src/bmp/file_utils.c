#include "bmp.h"
#include <stdio.h>

enum open_status open_file_read( FILE** in, const char* filename ) {
    *in = fopen(filename, "rb+");
    if (in != NULL) {
        return OPEN_OK;
    }
    return OPEN_ERR;
}

enum open_status open_file_write( FILE** in, const char* filename ) {
    *in = fopen(filename, "wb+");
    if (in != NULL) {
        return OPEN_OK;
    }
    return OPEN_ERR;
}

enum close_status close_file( FILE* in ) {
    if (fclose(in) == 0) {
        return CLOSE_OK;
    }
    return CLOSE_ERR;
}

