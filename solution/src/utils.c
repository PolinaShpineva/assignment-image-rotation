#include "bmp.h"
#include "image.h"
#include <stdio.h>

uint64_t padding( const struct image* img ) {
    return (4 - sizeof(struct pixel) * (img->width) % 4) % 4;
}

void printf_read( enum read_status read ) {
    printf("%s", read_arr[read]);
}

void printf_write( enum write_status write ) {
    printf("%s", write_arr[write]);
}
