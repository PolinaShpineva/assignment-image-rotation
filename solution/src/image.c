#include "image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>


/*struct image create_img( uint64_t width, uint64_t height ) {
    struct image img = {
        .width = width, 
        .height = height, 
        .data = malloc(width * height * sizeof(struct pixel))
    };
    return img;
}*/

void delete_img( struct image* img ) {
    free(img->data);
    img->data = NULL;
}
