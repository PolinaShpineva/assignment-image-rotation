#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct __attribute__((packed)) bmp_header 
{
        uint16_t bfType; // 0x4d42
        uint32_t  bfileSize; // size of file
        uint32_t bfReserved; // 0 for reserved
        uint32_t bOffBits; //
        uint32_t biSize; // header size, in bytes
        uint32_t biWidth; // image width, in pixels
        uint32_t  biHeight; // image height, in pixels
        uint16_t  biPlanes; // 1
        uint16_t biBitCount; // bits per pixel
        uint32_t biCompression; // 0 or 3 uncompressed
        uint32_t biSizeImage; // size in bytes
        uint32_t biXPelsPerMeter; // horizontal resolution
        uint32_t biYPelsPerMeter; // vertical resolution
        uint32_t biClrUsed; // 0 for the max number of colors
        uint32_t  biClrImportant; // 0 - all colors required
};
#pragma pack(pop)

uint64_t padding( const struct image* img );



enum open_status {
  OPEN_OK = 0,
  OPEN_ERR
};

enum close_status {
  CLOSE_OK = 0,
  CLOSE_ERR
};

enum open_status open_file_read( FILE** in, const char* filename );
enum open_status open_file_write( FILE** in, const char* filename );
enum close_status close_file( FILE* in );

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_COLOR,
  READ_INVALID_HEADER_SIZE
};

enum read_status from_bmp( FILE* in, struct image* img );


/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR 
};

enum write_status to_bmp( FILE* out, struct image const* img );

void printf_read( enum read_status read );

static const char* read_arr[] = {
  [READ_INVALID_SIGNATURE] = "Incorrect file\n",
  [READ_INVALID_BITS] = "Incorrect abount of bits per pixel\n",
  [READ_INVALID_HEADER] = "Incorrect header\n",
  [READ_INVALID_COLOR] = "Incorrect amount of colors\n",
  [READ_INVALID_HEADER_SIZE] = "Incorrect amount of reqiured colors\n"
};

void printf_write( enum write_status write );

static const char* write_arr[] = {
    [WRITE_ERROR] = "Something goes wrong with writing to the file\n"
};

#endif //BMP_H


